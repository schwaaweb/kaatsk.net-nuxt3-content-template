// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: true,
  app: {
    baseURL: '/~darwin',
  },
  nitro: {
    static: true,
  },
  devtools: { enabled: true },
  modules: [
    '@nuxt/ui',
    '@nuxt/content',
    'nuxt-icon',
    '@nuxtjs/google-fonts',
    '@nuxtjs/tailwindcss'
  ],
  content:
    ['docs/content/**/*.md'],
  googleFonts:{
    families: {
      'IBM Plex Mono': true,
      'IBM Plex Sans': true,
      'IBM Plex Serif': true,
    }
  },
  css: ['assets/css/main.css'],
  ui: {
    icons: "all",
  },
});
