# Prerequisites

## Shell access

this is typically zsh on OSX or bash on linux (and windows with Windows Linux Subsystem )

So open whatever zsh or bash shell you have access to. You will need access to the `curl` command. You can check for this command once you open the shell/terminal by running:

```bash
which curl
```

## nvm
https://github.com/nvm-sh/nvm#installing-and-updating

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
```

## bun
```bash
curl -fsSL https://bun.sh/install | bash 
# bun upgrade # to keep this upgraded
```
you might need to open a new terminal after this runs


# Build
```bash
bunx nuxi@latest init kaatsk.net-template -t content ui
cd kaatsk.net-template
bun add -D @nuxt/ui @nuxt/content @nuxtjs/google-fonts nuxt-icon @iconify/json @nuxtjs/tailwindcss
bun install 
bun run dev
```

# text editor

You will need a text editor to make adjustments to your project.

VS Code can be downloaded: 

[HERE](https://code.visualstudio.com/download)


# Download the website code available:
[HERE](https://gitlab.com/schwaaweb/kaatsk.net-nuxt3-content-template)





# GitLab

Start an empty project in GitLab

From this empty project you'll get the information to run the commands below under import git repository

```bash
git remote add origin git@gitlab.com:schwaaweb/kaatsk.net-template.git
git push --set-upstream origin --all
git push --set-upstream origin --tags
```



# The files below is what I changed past the initial configuration

# nuxt.config.ts
```ts
// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: true,
  app: {
    baseURL: '/~darwin',
  },
  nitro: {
    static: true,
  },
  devtools: { enabled: true },
  modules: [
    '@nuxt/ui',
    '@nuxt/content',
    'nuxt-icon',
    '@nuxtjs/google-fonts',
    '@nuxtjs/tailwindcss'
  ],
  content:
    ['docs/content/**/*.md'],
  googleFonts:{
    families: {
      'IBM Plex Mono': true,
      'IBM Plex Sans': true,
      'IBM Plex Serif': true,
    }
  },
  css: ['assets/css/main.css'],
  ui: {
    icons: "all",
  },
});

```

# new folders

from the root directory of the repo run:

```bash
mkdir assets assets/css components components/content layouts
```

# assets/css/taildwind.css
```css
@tailwind base;
@tailwind components;
@tailwind utilities;


```
# assets/css/main.css
```css
body {
    font-family: "IBM Plex Serif";
  }
  
  h1 {
    font-family: "IBM Plex Mono";
  }
  
  p {
    font-family: "IBM Plex Sans";
  }
```

# tailwind.config.ts

```ts
/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./index.html", "./src/**/*.{vue,js,ts}"],
    theme: {
        extend: {},
    },
    plugins: [],
};
```
# layouts/default.vue
```vue
<template>
    <div>
        <div class="max-w-3xl px-4 mx-auto">
        <!-- header-->
            <header class="mt-16 mb-4 flex justify-between items-end">
                <div>
                    <NuxtLink to="/"><h1 class="text-2xl font-semibold">http://kaatsk.net/~darwin 's TEMPLATE</h1></NuxtLink>
                    <h4 class="text-gray-500"><strong>your</strong> creative webhome in Delhi, NY</h4>
                </div>
                <Btn>Do Something!</Btn>
            </header>
            <div class="max-w-3xl px-4 pb-3">
                <nav class="mx-auto mt-5">
                    <ul class="flex gap-4">
                        <li><NuxtLink to="http://kaatsk.net/">kaatsk.net</NuxtLink></li>
                        <li><NuxtLink to="/">Home</NuxtLink></li>
                        <li><NuxtLink to="/about">About</NuxtLink></li>
                    </ul>
                </nav>
            </div>
        <!--- ./header -->
    
        <!--- Main -->
            <div>
                <slot />
            </div>
        <!--- ./Main -->
        </div>
            <footer class="max-w-3xl mx-auto px-4">
                <nav class="container mx-auto p-4 flex justify-between">
                    <ul class="flex gap-4">
                        <li><NuxtLink to="http://kaatsk.net/">kaatsk.net</NuxtLink></li>
                        <li><NuxtLink to="/">Home</NuxtLink></li>
                        <li><NuxtLink to="/about">About</NuxtLink></li>
                    </ul>
                </nav>
            </footer>
        </div>
    </template>
    
    <style scoped>
        .router-link-exact-active {
            color: #12b488;
        }
    
    
    </style>
```



# pages/[...slug].vue
```vue
<template>
  <main>
    <ContentDoc />
  </main>
</template>

<style>
  p { padding: 8px; }

  h1 { font-weight: 800; }
</style>
```

# components/content/ContentBtn.vue
```vue
<script lang="ts" setup></script>

<template>
    <button class="py-8 px-4 bg-white rounded-xl border my-2">
        <slot></slot>
    </button>

</template>

<style></style>
```

# components/Btn.vue
```vue
<script lang="ts" setup></script>

<template>
    <button class="py-2 px-4 bg-orange rounded-xl border my-2">
        <slot></slot>
    </button>

</template>

<style></style>
```
