# Prerequisites

## Shell access

this is typically zsh on OSX or bash on linux (and windows with Windows Linux Subsystem )

So open whatever zsh or bash shell you have access to. You will need access to the `curl` command. You can check for this command once you open the shell/terminal by running:

TODO: How to find the shell on ubuntu, osx & windows

We will need these programs installed and accessible from your shell. You can test if they are there like this:
```bash
which curl git rsync
```
You should see shell output similar to this:

```bash
/usr/bin/curl
/usr/bin/git
/usr/bin/rsync
```
## nvm
https://github.com/nvm-sh/nvm#installing-and-updating

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
```

## bun
```bash
curl -fsSL https://bun.sh/install | bash 
# bun upgrade # to keep this upgraded
```
you might need to open a new terminal after this runs

# Create an account on kaatsk.net

[Directions are here.](http://kaatsk.net)

after you establish an account on **kaatsk.net** you can continue with the directons below.

You will need to know your home directory in order to continue. My home directory is located at: (http://kaatsk.net/~darwin/)[http://kaatsk.net/~darwin/] the last section of the url is your unique home directory. In my case this is `~darwin`. There are places in the website template source code that have `~darwin` that you should change from `~darwin` to whatever your home directory is. 

for example: 

If your name is Harriet and you choose to name your home directory after yourself your home directory would be at *http://kaatsk.net/~harriet/* and you would replace any occurence of `~darwin` in the source code with `~harriet`.



# text editor

You will need a text editor to make adjustments to your project.

VS Code can be downloaded: 

[HERE](https://code.visualstudio.com/download)

VS Code can be a nice place to start as a first introduction to text editors **but** [vim](https://www.freecodecamp.org/news/vim-beginners-guide/), [emacs](https://www.masteringemacs.org/article/beginners-guide-to-emacs) & [nano](https://www.nano-editor.org/dist/latest/cheatsheet.html) are available on the server and as your knowledge and skill grow will oftern be the preferred method of editing code.


# Download the website code available:
[HERE](https://gitlab.com/schwaaweb/kaatsk.net-nuxt3-content-template)


## if you have git installed ... you can run:
```bash
# everything on this line is a comment
git clone git@gitlab.com:schwaaweb/kaatsk.net-nuxt3-content-template.git && 
# the above line downloads the repo or repository of code
cd kaatsk.net-nuxt3-content-template &&  # this command brings you inside of the repo
pwd &&  # this command let's you know what directory you're in
ls      # this line ls or lists what files are in the repository directory
ls      # this line will also ls things in the sub-directories      
```
## installing software so that we can run our local development environment

```bash
cd kaatsk.net-nuxt3-content-template
bun install     # this installs packages that the development environment needs to run
bun run ddev    # this starts a development server at http://localhost:3000/~darwin/
```

## http://localhost:3000/~darwin/

So this **local** web address is my home directory. You're going to want to change this to *your* home directory. You can make this change in the repo's source code. In order to edit this source code you should start your text editor. 

## Starting VS Code

On many systems you can run `code .` on the command-line from within the repo's directory at `kaatsk.net-nuxt3-content-template` the command `code` calls VS Code  and the `.` simply means point VS Code at this directory that we're in right now.

I'm going to assume you're using VS Code for the purpose of this tutorial. You should be able to adapt these instructions using whatever text editor you have chosen. One of the main differences is that you will open the `kaatsk.net-nuxt3-content-template` folder in VS Code where you may have to open individual files in your text editor depending on how it is set up.

## Changing your document root

So this repo is set up to point to my home of `http://kaatsk.net/~darwin`. On my local development machine the repo is set up to display a test web page at `http://localhost:3000/~darwin`

In the case of `harriet` whom made an account on `http://kaatsk.net` at `http://kaatsk.net/~harriet` we want her development machine to point to `http://localhost:3000/~harriet`

In order for us to make this happen we need to *edit* a file in the root of the repo called `nuxt.config.ts`. In VS Code you should be able to click on this filename in the sidebar on the left side of the program screen. If a list of files is not visible you might have to click on the icon showing overlapping pieces of paper near the top left of the scree. This will open up the sidebar.

look for this code snippet near the top of the `nuxt.config.ts` file:
```ts
app: {
    baseURL: '/~darwin',
  },
  ```

  now *edit* then save the file to look like this:

  ```ts
app: {
    baseURL: '/~harriet',
  },
  ```

  This change should be visible at `http://localhost:3000/~harriet` now. If not go to the shell where you have `bun run dev` running and do a `CTRL-c` (This means hold the `CTRL` and then type the letter `c`) to stop the development server from running. Start the development server again with `bun run dev`. It should indicate that web page is active at `http://localhost:3000/~harriet`

  # Congratulations! 

  You have made your first step in customizing this repo for yourself!!
  


