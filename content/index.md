# kaatsk.net/~darwin  Nuxt Content Template

The code for this website is available [here](https://gitlab.com/schwaaweb/kaatsk.net-nuxt3-content-template) for you to download and customize.

# build.md

The instructions for using this template are here [/build](/build)


# index.md
`content/index.md`

This page located in the [code repository](https://gitlab.com/schwaaweb/kaatsk.net-nuxt3-content-template) at `content/index.md` corresponds to the `/` route of your website. 

You can delete it or create another file in the `content/` directory. You can use this repo here to create a website with *markdown* files.

This template is designed to run on [kaatsk.net](https://kaatsk.net/)

# about.md
`content/about.md`

Put whatever you want here. This *markdown* file is also located in the `content` directory.


Try to navigate to [/about](/about). These 2 pages are rendered by the `pages/[...slug].vue` component.

---

Look at the [Content documentation](https://content.nuxtjs.org/) to learn more.
